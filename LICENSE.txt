LICENCE

Identité :
Cyril Iaconelli
Landéhen
cyril.iaconelli@ac-rennes.fr

Mentions relatives à l’hébergement du site :
Ce site est hébergé sur la plateforme forge.apps.education.fr, qui est sous la responsabilité du Ministère de l'Éducation Nationale.
Hébergeur : ONLINE SAS, BP 438, 75366 PARIS CEDEX 08

Mentions liées aux sites d’information :
Responsable du site : Cyril Iaconelli
Contact : cyril.iaconelli@ac-rennes.fr
Hébergeur : ONLINE SAS, BP 438, 75366 PARIS CEDEX 08

Mentions relatives à la propriété intellectuelle :
Les documents publiés sur ce site sont disponibles sous les licences suivantes :
- **CC-BY-SA** : Les utilisateurs peuvent utiliser, modifier et distribuer les documents, à condition de créditer l’auteur et de distribuer les œuvres dérivées sous la même licence.
- **CC-BY** : Certains documents peuvent être simplement crédités, sans obligation de redistribution sous la même licence.

Les codes publiés sur ce site sont disponibles sous la licence **CC-BY**. Les utilisateurs peuvent les utiliser, modifier et redistribuer, même à des fins commerciales, à condition de créditer l’auteur.

Présentation et description des services :
Le site propose des ressources pédagogiques et du code source libre de droits. Les utilisateurs peuvent utiliser, modifier et partager ces ressources en respectant les termes des licences CC-BY et CC-BY-SA, en fonction du type de contenu.

Dispositifs relatifs à la propriété intellectuelle et obligations des utilisateurs :
Les utilisateurs s’engagent à respecter les termes des licences CC-BY et CC-BY-SA, notamment en créditant l’auteur lorsqu’ils utilisent ou distribuent les contenus disponibles sur ce site. Toute infraction à ces règles pourra entraîner des sanctions telles que la suspension des droits d'accès aux services du site.

Responsabilité de l’éditeur du site :
- L'éditeur du site ne peut être tenu pour responsable du contenu republié par des tiers à partir d’un autre site.
- L'éditeur ne peut être tenu pour responsable des propos insultants, diffamants ou mensongers tenus par les utilisateurs sur les forums, chats ou autres espaces de discussion.
- L'éditeur ne garantit pas un accès continu et sans interruption au site. Des bugs ou des interruptions peuvent survenir pour permettre des opérations de maintenance.

Politique d’utilisation des données personnelles :
Ce site n’utilise aucune base de données, ne stocke ni ne traite aucune donnée personnelle des visiteurs.

Cookies :
Ce site utilise des cookies strictement nécessaires à la navigation, déposés exclusivement par l’éditeur du site. Ces cookies sont indispensables au bon fonctionnement du site et ne nécessitent pas le consentement préalable des utilisateurs.

Droit applicable et juridiction compétente :
Le site et ses contenus sont soumis à la législation française. En cas de litige, seuls les tribunaux français seront compétents.
