# Utilisation de $\LaTeX$ et [MathAlea](https://coopmaths.fr/alea/) pour réaliser des fiches d'exercices de type Pixel Art


**Table des matières**
1. [Introduction](#introduction)
2. [La vidéo](#video)
3. [Packages supplémentaires](#packages)
4. [Chargement de la grille](#chargement)
5. [Affichage de la grille](#affichage)
6. [Tableau des couleurs et valeurs](#tableau)
7. [Exemple complet simple - Sans Mathalea](#exemple)

<p id="introduction"></p>
## Introduction

[MathAlea](https://coopmaths.fr/alea/) est un excellent outil pour générer des exercices de mathématiques personnalisés. Dans ce billet, nous allons voir comment utiliser cette application pour créer des fiches d'exercices de type Pixel Art en modifiant et en ajoutant quelques éléments supplémentaires au code $\LaTeX$ généré par MathAlea.

<p id="video"></p>
## La vidéo

<div class="iframe-container">
    <iframe title="Créer des fiches d'exercices type Pixel Art - #3 La Fiche sur LaTeX" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/e4f7809a-f8b0-46db-b13e-f3f6c8ac9d30"></iframe>
</div>

<p id="packages"></p>
## Packages supplémentaires

Pour commencer, nous devons ajouter deux packages essentiels pour la création de Pixel Art : `couleurs-fr` et `PixelArtTikz`. Ajoutez les lignes suivantes au préambule de votre document LaTeX :

```latex
%%% Packages supplémentaire de Cédric Pierquet
\usepackage{couleurs-fr}
\usepackage{PixelArtTikz}

%%% Autres packages nécessaire
\usepackage{enumitem}
\usepackage{colortbl}
\uspepackage{icomma}
```

<p id="chargement"></p>
## Chargement de la grille

Ensuite, il faut préparer la grille de Pixel Art en format CSV. Cette grille doit être incluse en début de document LaTeX, avant le début du document proprement dit :

```latex
%%% Grille à coller en lieu et place de celle-ci
\begin{filecontents*}[overwrite]{NomDeLaGrille.csv}
A,A,B,B,B,B,B,B,A,A
A,B,C,C,C,C,C,C,B,A
B,C,C,C,C,C,C,C,C,B
B,C,B,B,C,C,B,B,C,B
B,C,B,A,C,C,A,B,C,B
B,C,C,C,C,C,C,C,C,B
B,C,B,C,C,C,C,B,C,B
B,C,C,B,B,B,B,C,C,B
A,B,C,C,C,C,C,C,B,A
A,A,B,B,B,B,B,B,A,A
\end{filecontents*}
```

Remplacez `NomDeLaGrille.csv` par le nom de votre fichier de grille et collez le contenu de votre grille CSV à l'emplacement indiqué.

## Utilisation de l'option `label=\fbox{\Alph*}`

Pour représenter la grille avec des lettres, il faut ajouter l'option `label=\fbox{\Alph*}` à l'environnement `enumerate`. Cela permet d'afficher les lettres dans des cases :

```latex
\begin{enumerate}[label=\fbox{\Alph*},itemsep=1em]
```

<p id="affichage"></p>
## Affichage de la grille

Enfin, nous chargeons la grille et l'affichons avec le bon nombre de lettres et les bonnes couleurs. Les deux lignes suivantes permettent de générer la grille :

```latex
%Pour afficher la grille avec des lettres sans couleur
\PixelArtTikz[Codes=ABC,Unite=0.5]{NomDeLaGrille.csv}
%Pour afficher la grille avec les couleur sans les lettres
\PixelArtTikz[Codes=ABC,Couleurs={Noir,Blanc,Gris},Correction,Unite=0.5]{NomDeLaGrille.csv}
```

La première ligne génère la grille avec les lettres, tandis que la deuxième ligne génère la grille avec les couleurs définies par le package `couleurs-fr`. Assurez-vous de remplacer `ABC` par les lettres correspondant à votre grille et d'ajuster les couleurs selon vos besoins.

<p id="tableau"></p>
## Tableau des couleurs et valeurs

Sous la grille, ajoutez le tableau suivant pour indiquer les couleurs et les valeurs correspondantes :

```latex
%Exemple de tableau
\begin{tabular}{|c|c|c|}
    \hline
    \cellcolor{Gris}Gris & %C
    \cellcolor{Blanc}Blanc & %A
    \cellcolor{Noir}\textcolor{Blanc}{Noir} \\ %B
    \hline
    $5$ & %C
    $2$ & %A
    $3$ \\ %B
    \hline
\end{tabular}
```

<p id="exemple"></p>
## Exemple complet simple - Sans Mathalea

![Copie d'écran de l'exemple](CopieMathalea.png)

Voici un exemple complet d'un document LaTeX utilisant MathAlea et les packages supplémentaires pour créer un exercice de type Pixel Art :

```
\documentclass{article}

%%% Packages indispensables
\usepackage{PixelArtTikz}
\usepackage{enumitem}
\usepackage{colortbl}
\usepackage{couleurs-fr}

%%% Chargement de la grille
\begin{filecontents*}[overwrite]{MaGrille.csv}
	A,A,B,B,B,B,B,B,A,A
	A,B,C,C,C,C,C,C,B,A
	B,C,C,C,C,C,C,C,C,B
	B,C,B,B,C,C,B,B,C,B
	B,C,B,A,C,C,A,B,C,B
	B,C,C,C,C,C,C,C,C,B
	B,C,B,C,C,C,C,B,C,B
	B,C,C,B,B,B,B,C,C,B
	A,B,C,C,C,C,C,C,B,A
	A,A,B,B,B,B,B,B,A,A
\end{filecontents*}

\begin{document}
	
	\section*{Fiche d'exercices - Pixel-Art}
	
	\subsection*{Consignes}
	
	Résoudre les équations ci-dessous et coloriez la grille selon les solutions obtenues.
	
	\begin{enumerate}[label=\fbox{\Alph*}]
		\item \( 2x + 3 = 7 \)
		\item \( 4x - 1 = 11 \)
		\item \( 3x - 4 = 11 \)
	\end{enumerate}
	
	\bigskip
	
	\begin{minipage}{0.48\textwidth}
	\PixelArtTikz[Codes=ABC,Unite=0.45]{MaGrille.csv}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\textwidth}
	\PixelArtTikz[Codes=ABC,Couleurs={Blanc,Noir,Gris!50},Correction,Unite=0.45]{MaGrille.csv}
	\end{minipage}
	
	\bigskip
	
	
	\begin{center}
	\begin{tabular}{|c|c|c|}
		\hline
		\cellcolor{Gris}Gris & %C
		\cellcolor{Blanc}Blanc & %A
		\cellcolor{Noir}\textcolor{Blanc}{Noir} \\ %B
		\hline
		\(5\) & %C
		\(2\) & %A
		\(3\) \\ %B
		\hline
	\end{tabular}
	\end{center}
	
\end{document}
```

En suivant ces étapes, vous pourrez créer des fiches d'exercices de type Pixel Art personnalisées et attrayantes pour vos élèves.
Bonne création !
