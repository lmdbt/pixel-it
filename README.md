# <img style="width:30% ; box-shadow: 10px 10px 5px rgba(0, 0, 0, 0.5);" src="logo-pixel-it2.png">

Pixel-It est une application web conçue pour transformer l'art du pixel en une expérience éducative et divertissante. Idéale pour les enseignants, les étudiants et tous les passionnés d'art numérique, Pixel-It vous aide à transformer des œuvres et dessins en pixel art à colorier ! :space_invader:

![Copie d'écran de la page web](pixel-it-screenshot.png)

## Fonctionnalités :mag:

- **Grille Personnalisable :** Les utilisateurs peuvent définir les dimensions de leur grille pour adapter leur projet à leurs besoins.
- **Sélection de Couleur :** Une palette pour choisir et appliquer des couleurs à chaque pixel, avec un code automatique attribué à chaque couleur sélectionnée.
- **Image de Fond :** Importez une image de référence pour vous guider dans votre création de pixel art.

## Illustrations et exemple :art:

![Exemple Renard Pixel Art](PixelIt-Illustration1.png)
<p style="text-align:center;">*De gauche à droite : Image d’origine, image chargée en image de fond, pixel art en cours de réalisation, pixel art terminé, grille de lettres obtenue et résultat en couleur une fois colorié.
Source de l’image d’origine : [Ada & Zangemann](https://cfeditions.com/ada/)*</p>

Découvrez un exemple de cet exercice ici : [Exemple d'exercice Pixel-It](https://nuage01.apps.education.fr/index.php/s/8nHwfGSDnQs7kYz)

![Exemple Renard Pixel Art](exemple-renard.png)
<p style="text-align:center;">*Illustration de l'exercice Pixel-It avec le pixel art d'un renard.*</p>

Le logo de Pixel-It a lui même été réalisé avec Pixel-It ! :smile:

## Utilisation pédagogique :school:

Pixel-It offre une méthode interactive pour enseigner et apprendre le concept de codage des couleurs, favorisant la compréhension des relations entre les codes et les couleurs. Les enseignants peuvent créer des exercices ludiques et éducatifs en utilisant la grille de pixel art comme support.

- **Conception de l'exercice :** Créez une grille où chaque pixel est associé à un code numérique, invitant les élèves à découvrir la couleur correspondante.
- **Exportation et Préparation :** Exportez cette grille en format CSV pour l'intégrer dans des documents pédagogiques ou des tableurs.
- **Réalisation par les élèves :** Les élèves utilisent les codes comme indices pour remplir la grille avec les couleurs correctes, développant ainsi leurs compétences analytiques et leur compréhension du traitement numérique.

## Commencer avec Pixel-It :pencil2:

1. **Définissez la Taille de la Grille :** Commencez par créer et dimensionner votre grille.
2. **Sélectionnez vos Couleurs :** Choisissez les couleurs à appliquer sur chaque pixel (ou utiliser la fonction "picker" pour reprendre une couleur déjà utilisée).
3. **Peignez votre Grille :** Remplissez les pixels pour réaliser votre œuvre d'art.
4. **Enregistrez et Reprenez :** Sauvegardez votre travail en HTML et reprenez-le à tout moment (pensez à réaffecter les couleurs avec les lettres avant de reprendre votre Pixel Art).
5. **Exportez en CSV :** Convertissez votre création en CSV pour une utilisation ou une analyse ultérieure sur tableur ou $\LaTeX$ (notamment via [PixelArtTikz](https://ctan.org/pkg/pixelarttikz)).


Utilisez Pixel-It pour transformer vos idées en œuvres d'art pixelisées et partagez votre créativité ! :star:
